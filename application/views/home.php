<?php $this->load->view('components/page_head'); ?>

<!--Main Heading -->
<div class="banner">
    <div class="container">
        <div class="bnr-text">
            <h1>Come,Play & Win</h1>

        </div>
    </div>
</div>

<!-- Content -->
<section id="content">

    <div class="content-wrap-home">

        <div class="container clearfix">

            <div class="fancy-title title-center title-dotted-border topmargin">
                <h3>Upcoming Quiz</h3>
            </div>
            <?php if(empty($quiz)): ?>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                        <div class="promo promo-border promo-mini center">
                            <h3>No Upcoming Quizzes.<br>Try our free quizzes!</h3>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <div id="oc-events" class="owl-carousel events-carousel">

                    <?php
                    $count = 0;
                    foreach ($quiz as $qz) {
                        if($count % 2 == 0) {
                            echo '<div class="oc-item">';
                        }
                        
                        $count += 1;

                        echo '<div class="ievent clearfix">';
                        echo '<div class="entry-image">';
                        echo '<a href="'.site_url('quiz/'.$qz->slug).'">';
                        if($qz->cost == 0) {
                            echo '<img src="'.site_url('img/quiz-sponsored').'" alt="'.$qz->title.'">';
                        } else {
                            echo '<img src="'.site_url('img/quiz-paid').'" alt="'.$qz->title.'">';                                    
                        }
                        $time = strtotime($qz->date);
                        echo '<div class="entry-date">'.date('j', $time).'<span>'.date('M', $time).'</span></div>';
                        echo '</a>';
                        echo '</div>';
                        echo '<div class="entry-c">';
                        echo '<div class="entry-title">';
                        echo '<h2><a href="'.site_url('quiz/'.$qz->slug).'">'.$qz->title.'</a></h2>';
                        echo '</div>';
                        echo '<ul class="entry-meta clearfix">';
                        if($qz->cost == 0) {
                            echo '<li><span class="label label-primary">Sponsored</span></li>';
                        } else {
                            echo '<li><span class="label label-warning">Paid</span></li>';
                        }
                        echo '<li><a href="#"><i class="icon-time"></i> '.substr($qz->start_time, 0, 5).' - '.substr($qz->end_time, 0, 5).'</a></li>';
                        echo '<li><a href="#"><i class="icon-rupee"></i> '.$qz->prize_money.'</a></li>';
                        echo '</ul>';
                        echo '</div>';
                        echo '</div>';

                        if($count % 2 == 0) {
                            echo '</div>';
                        }
                    }
                    if($count % 2 != 0) {
                        echo '</div>';
                    }
                    ?>

                </div>

                <script type="text/javascript">

                    jQuery(document).ready(function($) {

                        var ocEvents = $("#oc-events");

                        ocEvents.owlCarousel({
                            margin: 20,
                            nav: true,
                            navText: ['<i class="icon-angle-left"></i>','<i class="icon-angle-right"></i>'],
                            autoplay: false,
                            autoplayHoverPause: true,
                            dots: false,
                            responsive:{
                                0:{ items:1 },
                                1000:{ items:2 }
                            }
                        });

                    });

                </script>
            <?php endif; ?>
        </div>
        
    </div>

    <div class="content-wrap" style="padding-top: 45px;">

        <div class="container clearfix">

            <div class="clear"></div>

            <div class="col_half">

                <div class="fancy-title title-center title-dotted-border">
                    <h3>Question Bank</h3>
                </div>

                <ul class="list-group">
                    <?php
                    foreach ($tags as $t) {
                        echo '<li class="list-group-item">';
                        echo '<span class="badge"><a href="'.site_url('question_bank/'.$t->name).'">View</a></span>';
                        echo ucwords(str_replace('-', ' ', $t->name));
                        echo '</li>';
                    }
                    ?>
                </ul>

            </div>

            <div class="col_half col_last">

                <div class="fancy-title title-center title-dotted-border">
                    <h3>Current Affairs</h3>
                </div>


                <div id="post-list-footer">
                    
                    <?php 
                    foreach ($current_affairs as $cf) {
                        echo '<div class="spost clearfix">';
                        echo '<div class="entry-c">';
                        echo '<div class="entry-title">';
                        echo '<h4><a href="'.site_url('current_affairs/'.$cf->id).'">'.$cf->title.'</a></h4>';
                        echo '</div>';
                        echo '<ul class="entry-meta">';
                        $time = strtotime($cf->date);
                        echo '<li>'.date('jS M, Y', $time).'</li>';
                        echo '</ul>';
                        echo '</div>';
                        echo '</div>';
                    }
                    ?>

                </div>

            </div>

        </div>

    </div>

</section>




    <div class="care">
        <div class="container">
            <div class="care-top">
                <h3>OneIndia Quiz</h3>
                <P>To connect people with General Knowledge & Current Affairs. Stay tuned for the latest news and updates.</P>
            </div>
            <div class="care-bottom">
                
                <div class="col-md-4 c-bottom">
                    <div class="care-bottom-right">
                        <img src="images/tree-2.png" alt="">
                        <h4>Upcoming Events</h4>
                        <p> WE ARE REALLY EXCITED ABOUT THE UPCOMING EVENTS</p>
                        
                    </div>
                    <div class="view">
                            <a href="quiz">VIEW</a>
                        </div>
                </div>
                <div class="col-md-4 c-bottom">
                    <div class="care-bottom-left">
                        <img src="images/tree.png" alt="">
                        <h4>GK Bank</h4>
                           <p> Basic General knowledge questions with answers.</p>
                                    <p></p>
                                    <p></p>
                                    <p></p>
                                    <p>SUCCESS IS WHERE PREPARATION AND OPPORTUNITY MEET</p>
                        
                    </div>
                    <div class="view">
                            <a href="question_bank">VIEW</a>
                        </div>
                </div>
                
                <div class="col-md-4 c-bottom">
                    <div class="care-bottom-middle">
                        <img src="images/tree-1.png" alt="">
                        <h4>Current Affairs</h4>
                        <p> COVERS A WIDER ASPECT OF DAY TO DAY ACTIVITIES UNDER "NATIONAL, INTERNATIONAL, REGIONAL, ECONOMY, SCIENCE AND TECHNOLOGY, ENVIRONMENT, INDIA AND WORLD" HEADS</p>
                        
                    </div>
                    <div class="view">
                            <a href="current_affairs">VIEW</a>
                        </div>
                </div>
                
                



                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div class="news">
        <div class="container">
            <div class="news-top">
                <h3>NEWS AND ARTICLES</h3>
            </div>
            <div class="news-bottom">
                <div class="col-md-6 news-bottom-left">
                    <a href="https://goo.gl/MxVvEq" target="blank" class="b-link-stripe b-animate-go  thickbox">
                        <img class="port-pic" class="img-responsive" src="images/one-2.jpg" />
                        <div class="b-wrapper">
                            <h2 class="b-animate b-from-left b-from   b-delay03 ">
                                <span></span>
                                <button >Participate</button>
                            </h2>
                        </div>
                    </a>
                </div>
                <div class="col-md-6 news-bottom-left">
                    <div class="news-btm">
                        <a href="" class="b-link-stripe b-animate-go  thickbox">
                            <img class="port-pic" src="images/apple.jpg" />
                            <div class="b-wrapper">
                                <h2 class="b-animate b-from-left    b-delay03 ">
                                    <button>View full article</button>
                                </h2>
                            </div>
                        </a>
                    </div>
                    <a href="single.html" class="b-link-stripe b-animate-go  thickbox">
                        <img class="port-pic" src="images/veg.jpg" />
                        <div class="b-wrapper">
                            <h2 class="b-animate b-from-left    b-delay03 ">
                                <button>View full article</button>
                            </h2>
                        </div>
                    </a>
                </div>
                <div class="clearfix"> </div>
                <div class="news-btm1">
                    <a href="single.html" class="b-link-stripe b-animate-go  thickbox">
                        <img class="port-pic" class="img-responsive" src="images/fd-btm1.jpg" />
                        <div class="b-wrapper">
                            <h2 class="b-animate b-from-left b-left   b-delay03 ">
                                <span></span>
                                <button>View full article</button>
                            </h2>
                        </div>
                    </a>
                </div>
                <div class="fd-btn">
                    <a href="blog.html">More</a>
                </div>
            </div>
        </div>
    </div>
    <div class="contact">
        <div class="container">
            <div class="contact-main">
                <div class="col-md-4 contact-left">
                    <h4>For Help center contact</h4>
                    <p><span>Email:</span><a href="mailto:@example.com">contact@oneindiaquiz.com</a></p>
                    <p><span>Phone:</span>(91) 9995 189 813</p>
                </div>
                <div class="col-md-4 contact-left">
                    <h4>Address</h4>
                    <p>Think Technologies,
                        Kinfra Industrial Park,
                        Thalassery, Kerala,
                        India - 670107</p>
                    </div>
                    <div class="col-md-4 contact-left">
                        <h4>Follow us</h4>
                        <ul>
                            <li><a href="https://www.facebook.com/oneindiaquiz"><span class="fb"> </span></a></li>
                            <li><a href="#"><span class="twit"> </span></a></li>
                            <li><a href="#"><span class="in"> </span></a></li>
                            <li><a href="#"><span class="yt"> </span></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>

        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3905.996541239903!2d75.51223271475064!3d11.765294191655205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba4263d6048fb4d%3A0x2e2d63c5e6cff1f8!2sKinfra+small+industries+park!5e0!3m2!1sen!2sin!4v1444369042609" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

<?php $this->load->view('components/page_tail'); ?>