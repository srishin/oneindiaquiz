<!DOCTYPE html> 
<html>
<head>
    <title><?php echo $title; ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <meta name="keywords" content="Bootstrap Responsive Templates, Iphone Compatible Templates, Smartphone Compatible Templates, Ipad Compatible Templates, Flat Responsive Templates"/>
    <script src="js/jquery-1.11.0.min.js"></script>
    <link href="<?php echo base_url('css/bootstrap.css')?>" rel='stylesheet' type='text/css' />
     <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.css'); ?>" type="text/css" />
    <link href="<?php echo base_url('css/style-old.css')?>" rel='stylesheet' type='text/css' /> <link href="<?php echo base_url('css/style.css')?>" rel='stylesheet' type='text/css' />
    <link href='http://fonts.googleapis.com/css?family=Arimo:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url('css/dark.css'); ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('css/font-icons.css'); ?>" type="text/css" />
      <link rel="stylesheet" href="<?php echo base_url('css/responsive.css'); ?>" type="text/css" />
</head>
<body>
    <!--start-header-->
    <div id="home" class="header">
        <div class="top-header">
            <div class="container">
                <div class="logo">
                    <a href="."><img src="<?php echo base_url('images/logo.png')?>" alt=""></a>  
                </div>
                <!--start-top-nav-->
                <div class="top-nav">
                    <ul>
                        <li class="active"><a class="play-icon popup-with-zoom-anim" href="<?php echo site_url('user/login')?>"><span> </span>Log in</a></li>
                        <li><a class="play-icon popup-with-zoom-anim" href="<?php echo site_url('user/sign_up'); ?>">Sign up</a></li>
                    </ul>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!---pop-up-box---->
        <script type="text/javascript" src="<?php echo site_url('js/modernizr.custom.min.js')?>"></script>    
        <link href="<?php echo site_url('css/popup-box.css')?>" rel="stylesheet" type="text/css" media="all"/>
        <script src="<?php echo site_url('js/jquery.magnific-popup.js')?>" type="text/javascript"></script>
        <!---//pop-up-box---->
        <div id="small-dialog" class="mfp-hide">
            <div class="login">
                <h2>Log In</h2>
                <h4>Already a Member</h4>
            </div>
        </div>
        <div id="small-dialog1" class="mfp-hide">
            <div class="signup">
                <h1>Sign Up</h1>
                <h4>Enter Your Details Here</h4>
            </div>
        </div>  
        <script>
            $(document).ready(function() {
                $('.popup-with-zoom-anim').magnificPopup({
                    type: 'inline',
                    fixedContentPos: false,
                    fixedBgPos: true,
                    overflowY: 'auto',
                    closeBtnInside: true,
                    preloader: false,
                    midClick: true,
                    removalDelay: 300,
                    mainClass: 'my-mfp-zoom-in'
                });

            });
        </script>                   
        <!--End-header-->
        <div class="navgation">
            <div class="menu">
                <a class="toggleMenu" href="#"><img src="<?php echo site_url('images/menu-icon.png'); ?>" alt="" /> </a>
                <ul class="nav" id="nav">
                    <li><a href="<?php echo site_url(); ?>" class="active">Home</a></li>
                    <li><a href="<?php echo site_url('about_us'); ?>">About Us</a></li>
                    <li><a href="<?php echo site_url('quiz'); ?>">Quiz</a></li>
                    <li><a href="<?php echo site_url('question_bank'); ?>">Question Bank</a></li>
                    <li><a href="<?php echo site_url('current_affairs'); ?>">Current Affairs</a></li>
                </ul>
                <!----start-top-nav-script---->
                <script type="text/javascript" src="<?php echo site_url('js/responsive-nav.js'); ?>"></script>
                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $(".scroll").click(function(event){     
                            event.preventDefault();
                            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                        });
                    });
                </script>
                <!----//End-top-nav-script---->
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>