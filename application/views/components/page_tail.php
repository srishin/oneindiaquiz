<div class="footer">
    <div class="container">
        <div class="footer-text">
            <p>&copy; 2016 OneIndiaQuiz . All Rights Reserved </p>
        </div>
    </div>
    <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
</div>

<script type="text/javascript" src="<?php echo site_url('js/functions.js'); ?>"></script>

<?php if ($this->session->flashdata('notify') != FALSE): ?>
    <script>
        $(document).ready(function() {
            var position = <?php echo ($this->session->flashdata('n_position') != FALSE)? json_encode($this->session->flashdata('n_position')) :json_encode('top-right'); ?>,
            type = <?php echo ($this->session->flashdata('n_type') != FALSE)? json_encode($this->session->flashdata('n_type')) : json_encode('info'); ?>,
            message = <?php echo json_encode($this->session->flashdata('n_message')); ?>,
            close = <?php echo ($this->session->flashdata('n_close') != FALSE)? json_encode('true'): json_encode('false'); ?>;

            SEMICOLON.widget.notifications(position, type, message, close);
        });
    </script>
<?php endif; ?>

</body>
</html>