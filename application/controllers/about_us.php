<?php
class About_Us extends Frontend_Controller
{    
    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['current_page'] = 'About Us';
        $this->data['title'] = 'About Us | One India Quiz';
		
		$this->load->view('about_us', $this->data);
    }
}
?>